package taller;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Propietari implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id @GeneratedValue
    private long id;

    private String nom;
    private LocalDate dataNaixement;
    private int anys;
    
    public Propietari() {
    	
    }
    
	public Propietari(String nom, LocalDate dataNaixement, int anys) {
		super();
		this.nom = nom;
		this.dataNaixement = dataNaixement;
		this.anys = anys;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public LocalDate getDataNaixement() {
		return dataNaixement;
	}
	public void setDataNaixement(LocalDate dataNaixement) {
		this.dataNaixement = dataNaixement;
	}
	public int getAnys() {
		return anys;
	}
	public void setAnys(int anys) {
		this.anys = anys;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Propietari [id=" + id + ", nom=" + nom + ", dataNaixement=" + dataNaixement + ", anys=" + anys + "]";
	}
    
    

}