//-Heu de desenvolupar una aplicaci� que permeti fer el manteniment dels vehicles que gestiona una taller de cotxes. 
//-La informaci� s'ha d'emmagatzemar en una bdoo que, com a m�nim, ha de contenir objectes de tipus vehicles i de tipus propietaris.
//-Identifiqueu per a cada tipus d'objecte els camps necessaris i teniu en compte que, entre tots els objectes que emmagatzemareu en la 
// bdoo, hi ha d'haver, com a m�nim, un camp de tipus String, un de tipus data, un de tipus enter, i un de tipus boole�.
//-La interf�cie de l'aplicaci� �s lliure, per� �s valorar� m�s l'�s d'una interf�cie visual enlloc de text, i la usabilitat, la coher�ncia i 
// l'ordre de la interf�cie tamb� comptaran m�s.
//-L'aplicaci� ha de permetre llistar les dades dels vehicles, les dades dels propietaris i les dades de la resta d'objectes que emmagatzemeu en la bdoo.

package taller;

 
import javax.persistence.*;
import javax.servlet.*;
import javax.servlet.annotation.WebListener;
 
@WebListener
public class Taller implements ServletContextListener {

    // Prepare the EntityManagerFactory & Enhance:
    public void contextInitialized(ServletContextEvent e) {
        com.objectdb.Enhancer.enhance("taller.*");
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("$objectdb/db/db9.odb");
        e.getServletContext().setAttribute("emf", emf);
    }
 
    // Release the EntityManagerFactory:
    public void contextDestroyed(ServletContextEvent e) {
        EntityManagerFactory emf =
            (EntityManagerFactory)e.getServletContext().getAttribute("emf");
        emf.close();
    }
}