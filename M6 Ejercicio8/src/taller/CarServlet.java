package taller;
 
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.persistence.*;
 
@WebServlet("/CarServlet")
public class CarServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
 
    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        // Nos conectamos a la base de datos:
        EntityManagerFactory emf =
           (EntityManagerFactory)getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
 
        try {
            //Recibimos todos los parametros y los guardamos en una variable
        	String paramPropietari = request.getParameter("propietariId");
        	String paramMarca = request.getParameter("marca");
        	String paramDataAdquisicio = request.getParameter("dataAdquisicio");
        	String paramSegonaMa = request.getParameter("segonaMa");
        	String paramAnys = request.getParameter("anys");
        	
			//Si todo lo que pido por parametro llega entrara en el if y a�adira el contenido a la base de datos
            if (paramPropietari != null && paramMarca != null && paramDataAdquisicio != null && paramAnys != null) {
            	Propietari propietari = em.createQuery("SELECT g FROM Propietari g where id = " + paramPropietari, Propietari.class).getSingleResult();
                String marca = paramMarca;
                LocalDate dataAdquisicio = LocalDate.parse(paramDataAdquisicio);
                boolean segonaMa = paramSegonaMa != null;
                //Controla si esta vacio
                String controlAnys = paramAnys.equalsIgnoreCase("")? "0": request.getParameter("anys");
                int anys = Integer.parseInt(controlAnys);

				//Empieza la transaccion 
                em.getTransaction().begin();
				//A�ado el objeto a la base de datos
                em.persist(new Car(propietari, marca, dataAdquisicio, segonaMa, anys));
				//Empieza la transaccion 
                em.getTransaction().commit();
            }
 
            // Recuperamos todos los coches:
            List<Car> carList = em.createQuery(
                "SELECT g FROM Car g", Car.class).getResultList();
            request.setAttribute("cars", carList);

            
            // Recuperamos todos los propietarios:
            List<Propietari> propietariList = em.createQuery(
                "SELECT g FROM Propietari g", Propietari.class).getResultList();
            request.setAttribute("propietaris", propietariList);
            
            //Los enviamos a la pagina taller.jsp
            request.getRequestDispatcher("/taller.jsp")
                .forward(request, response);
 
        } finally {
            // Close the database connection:
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    protected void doPost(
        HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}