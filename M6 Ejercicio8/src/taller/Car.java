package taller;


import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Car implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id @GeneratedValue
    private long id;

    private Propietari propietari;
    private String marca;
    private LocalDate dataAdquisicio;
    private boolean segonaMa;
    private int anys;
    
    public Car() {
    	
    }

	public Car( Propietari propietari, String marca, LocalDate dataAdquisicio, boolean segonaMa, int anys) {
		super();
		this.id = id;
		this.propietari = propietari;
		this.marca = marca;
		this.dataAdquisicio = dataAdquisicio;
		this.segonaMa = segonaMa;
		this.anys = anys;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Propietari getPropietari() {
		return propietari;
	}

	public void setPropietari(Propietari propietari) {
		this.propietari = propietari;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public LocalDate getDataAdquisicio() {
		return dataAdquisicio;
	}

	public void setDataAdquisicio(LocalDate dataAdquisicio) {
		this.dataAdquisicio = dataAdquisicio;
	}

	public boolean isSegonaMa() {
		return segonaMa;
	}

	public void setSegonaMa(boolean segonaMa) {
		this.segonaMa = segonaMa;
	}

	public int getAnys() {
		return anys;
	}

	public void setAnys(int anys) {
		this.anys = anys;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public long getPropietariId() {
		return propietari.getId();
	}
	
	public String getPropietariNom() {
		return propietari.getNom();
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", propietari=" + propietari + ", marca=" + marca + ", dataAdquisicio="
				+ dataAdquisicio + ", segonaMa=" + segonaMa + ", anys=" + anys + "]";
	}
    

    
    
}

