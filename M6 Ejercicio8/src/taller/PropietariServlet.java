package taller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.persistence.*;

@WebServlet("/PropietariServlet")
public class PropietariServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//En caso de acceder por GET...
	@Override
	protected void doGet(
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {

		// Obtain a database connection:
		EntityManagerFactory emf =
				(EntityManagerFactory)getServletContext().getAttribute("emf");
		EntityManager em = emf.createEntityManager();

		try {
			// Handle a new guest (if any):
			String paramName = request.getParameter("nom");
			String paramDataNaixement = request.getParameter("dataNaixement");
			String paramAnys = request.getParameter("anys");
			
			//Si todo lo que pido por parametro llega entrara en el if y a�adira el contenido a la base de datos
			if (paramName != null &&  paramDataNaixement != null && paramAnys != null) {
				String name = paramName;
				LocalDate dataNaixement = LocalDate.parse(paramDataNaixement.equals("")?LocalDate.now().toString():paramDataNaixement);
				int anys = Integer.parseInt(paramAnys);
				
				//Empieza la transaccion 
				em.getTransaction().begin();
				//A�ado el objeto a la base de datos
				em.persist(new Propietari(name, dataNaixement, anys));
				//Finaliza la transaccion
				em.getTransaction().commit();
			}


            // Recuperamos todos los propietarios:
				List<Propietari> propietariList = em.createQuery(
						"SELECT g FROM Propietari g", Propietari.class).getResultList();
				request.setAttribute("propietaris", propietariList);

	            // Recuperamos todos los coches:
				List<Car> carList = em.createQuery(
						"SELECT g FROM Car g", Car.class).getResultList();
				request.setAttribute("cars", carList);

	            //Los enviamos a la pagina taller.jsp
				request.getRequestDispatcher("/taller.jsp")
				.forward(request, response);

		} finally {
			// Close the database connection:
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			em.close();
		}
	}

	//En caso de acceder por POST...
	@Override
	protected void doPost(
			HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
		doGet(request, response);
	}
}