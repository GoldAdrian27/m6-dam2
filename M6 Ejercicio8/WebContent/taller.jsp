<%@page import="taller.Propietari"%>
  <%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.*,taller.Car"%>

      <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

      <html>
        <head>
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
          <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
          <title>JPA car Book Web Application Tutorial</title>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <meta charset="UTF-8">
        </head>

        <body>
          <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="#">
              <img src="https://i.pinimg.com/originals/fc/6f/36/fc6f36ddfb95c4d658bed7d9157e0ffd.png" width="30" height="30" class="d-inline-block align-top" alt="">
              Taller de vehiculos
            </a>
          </nav>
          <div class="container">

            <div class="row my-3">
              <div class="col">
                <h2>Propietari</h2>
                <form method="POST" action="PropietariServlet">
                  <div class="form-group">
                    <input type="text" class="form-control" id="propietari-nom" name="nom" placeholder="Nom" required="required">
                  </div>

                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Data naixement</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" id="propietari-dataNaixement" max="9999-12-31" name="dataNaixement" placeholder="Data naixement" required="required">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Anys amb carnet</label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" name="anys" id="formGroupExampleInput2" placeholder="Anys amb carnet" required="required">
                    </div>
                  </div>

                  <input type="submit" value="Add"/>
                </form>
              </div>
              <div class="col">
                <h2>Cotxes</h2>
                <form method="POST" action="CarServlet">
                  <div class="form-group">
                    <select name="propietariId" required="required" class="form-control">
                      <%
                          @SuppressWarnings("unchecked")
                          List<Propietari> propietaris = (List<Propietari>)request.getAttribute("propietaris");
                           for (Propietari propietari : propietaris) { %>
                        <option value="<%= propietari.getId() %>">
                          Propietario
                          <%= propietari.getId() + " - Nombre: " + propietari.getNom() %>
                        </option>
                      <% } %>
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="marca" id="formGroupExampleInput" placeholder="Marca" required="required">
                  </div>

                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-4 col-form-label">Data Adquisicio</label>
                    <div class="col-sm-8">
                      <input type="date" class="form-control" name="dataAdquisicio" max="9999-12-31" id="formGroupExampleInput2" placeholder="Data Adquisicio" required="required">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="form-check">
                      <input class="form-check-input" name="segonaMa" type="checkbox" id="defaultCheck1">
                      <label class="form-check-label" for="defaultCheck1">
                        Segona ma?
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" name="anys" id="formGroupExampleInput2" placeholder="Anys de antiguetat" required="required">
                  </div>
                  <input type="submit" value="Add"/>
                </form>
              </div>
            </div>

            <div class="row my-3">
              <div class="col">
                <hr>
                <!-- Tabla de propietarios -->
                <table id="tabla_propietarios" class="display">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nom</th>
                      <th>Data Naixement</th>
                      <th>Anys amb carnet</th>
                    </tr>
                  </thead>
                  <tbody>
                    <% for (Propietari propietari : propietaris) { %>

                      <tr>
                        <td><%= propietari.getId() %></td>
                        <td><%= propietari.getNom() %></td>
                        <td><%= propietari.getDataNaixement() %></td>
                        <td><%= propietari.getAnys() %></td>
                      </tr>

                    <% } %>
                  </tbody>
                </table>
              </div>
              <div class="col">
                <hr>
                <!-- Tabla de coches -->
                <table id="tabla_coches" class="display">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Propietari</th>
                      <th>Marca</th>
                      <th>Adquisiscio</th>
                      <th>Segona ma</th>
                      <th>Anys</th>
                    </tr>
                  </thead>
                  <tbody>
                    <% @SuppressWarnings("unchecked")
                          List<Car> cars = (List<Car>)request.getAttribute("cars");
                          for (Car car : cars) { %>

                      <tr>
                        <td><%= car.getId() %></td>
                        <td><%= car.getPropietariId() + " - " + car.getPropietariNom() %></td>
                        <td><%= car.getMarca() %></td>
                        <td><%= car.getDataAdquisicio() %></td>
                        <td><%= car.isSegonaMa()?"Si":"No" %></td>
                        <td><%= car.getAnys() %></td>
                      </tr>

                    <% } %>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
          <script type="text/javascript">
            $(document).ready(function () {
              $('#tabla_propietarios').DataTable();
              $('#tabla_coches').DataTable();
            });
          </script>
        </body>
      </html>
