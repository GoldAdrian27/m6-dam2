package uf2.ejercicio8;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class Main {

	public static void main(String[] args) {
        // Open a database connection
        // (create a new database if it doesn't exist yet):
        EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("./src/uf2/ejercicio8/db/db8.odb");
        EntityManager em = emf.createEntityManager();

        // Guardar un propietario en la base de datos:
        em.getTransaction().begin();
        String[] nom = {"patata", "nairda", "jose"};
        
        for (int i = 0; i < 3; i++) {
            Propietari p = new Propietari(nom[i], LocalDate.now(), i);
            Car car = new Car(p, "Honda", LocalDate.now(), true, 4);
            em.persist(p);
            em.persist(car);
        }
        em.getTransaction().commit();

        //Select de propietarios
        System.out.println("PROPIETARIOS");
        Query q1 = em.createQuery("SELECT COUNT(p) FROM Propietari p");
        System.out.println("Total Cotxes: " + q1.getSingleResult());


        // Retrieve all the Point objects from the database:
        TypedQuery<Propietari> query =
            em.createQuery("SELECT p FROM Propietari p", Propietari.class);
        List<Propietari> results = query.getResultList();
        for (Propietari p : results) {
            System.out.println(p);
        }
        
        //Select de coches
        System.out.println("COCHES");
        Query q2 = em.createQuery("SELECT COUNT(c) FROM Car c");
        System.out.println("Total Propietaris: " + q2.getSingleResult());


        // Retrieve all the Point objects from the database:
        TypedQuery<Car> query2 =
            em.createQuery("SELECT c FROM Car c", Car.class);
        List<Car> results2 = query2.getResultList();
        for (Car car : results2) {
            System.out.println(car);
        }

        // Close the database connection:
        em.close();
        emf.close();
	}

}
