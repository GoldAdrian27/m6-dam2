package uf2.ejercicio1;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) throws SQLException {
		Connection con = null;
		Driver driver;
		String url = "jdbc:mysql://127.0.0.1:3306/m6_uf2_1";
		String usuari = "root";
		String password = "";
		Scanner teclado = new Scanner(System.in);
		boolean run = true;


		System.out.println("provaDeConexio()");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			con = DriverManager.getConnection(url, usuari, password);
			System.out.println("Connexió realitzada usant DriverManager");
		} catch (Exception e) {
			System.out.println("Error " + e.getMessage());
		}
		Statement selectStmt = con.createStatement();

		while (run) {
			System.out.println("1- Insertar");
			System.out.println("2- Eliminar");
			System.out.println("3- Actualizar");
			System.out.println("4- Listat");
			System.out.println("5- Seleccionar Uno");
			System.out.println("6- Salir");
			int num = teclado.nextInt();
			if (num == 1) {
				insertar(selectStmt);
			}else if(num == 2) {
				delete(selectStmt);

			}else if(num == 3) {
				update(selectStmt);

			}else if(num == 4) {
				list(selectStmt);

			}else if(num == 5) {
				listOne(selectStmt);
			}else{
				run = false;
			}
		}


	}

	public static void insertar(Statement selectStmt) throws SQLException {
		Scanner teclado = new Scanner(System.in);
		String nom;
		String dni;
		String data_naixement;
		String adreca_postal;
		String sexe;
		String codi_postal;
		String poblacio;

		System.out.println("Inserta el nombre: ");
		nom = teclado.next();
		System.out.println("Inserta el DNI: ");
		dni = teclado.next();
		System.out.println("Inserta el Fecha de nacimiento (yyyy/mm/dd): ");
		data_naixement = teclado.next();
		System.out.println("Inserta el Direccion : ");
		adreca_postal = teclado.next();
		System.out.println("Inserta el Sexo: ");
		sexe = teclado.next();
		System.out.println("Inserta el Codigo Postal: ");
		codi_postal = teclado.next();
		System.out.println("Inserta el Población: ");
		poblacio = teclado.next();

		selectStmt.execute("INSERT INTO alumnes (nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio) VALUES ('" + nom + "', '" + dni + "', '" + data_naixement + "', '" + adreca_postal + "', '" + sexe + "', '" + codi_postal + "', '" + poblacio + "')");

	}

	public static void delete(Statement selectStmt) throws SQLException {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Inserta la ID para buscar al alumno: ");
		int id = teclado.nextInt();

		selectStmt.execute("DELETE FROM alumnes where id = " + id);
	}

	public static void update(Statement selectStmt) throws SQLException {
		String nom;
		String data_naixement;
		String adreca_postal;
		String sexe;
		String codi_postal;
		String poblacio;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Inserta la ID para buscar al alumno: ");
		int id = teclado.nextInt();

		String[] columnas = list(selectStmt);

		System.out.println("Inserta el nombre[" + columnas[0] + "]: ");
		nom = teclado.next();
		nom = nom != ""?nom:columnas[0];
		
		System.out.println("Inserta el Fecha de nacimiento (yyyy/mm/dd)[" + columnas[1] + "]: ");
		data_naixement = teclado.next();
		data_naixement = data_naixement != ""?data_naixement:columnas[0];

		System.out.println("Inserta el Direccion" + columnas[2] + ": ");
		adreca_postal = teclado.next();
		adreca_postal = adreca_postal != ""?adreca_postal:columnas[0];

		System.out.println("Inserta el Sexo"+ columnas[3] + " : ");
		sexe = teclado.next();
		sexe = sexe != ""?sexe:columnas[0];

		System.out.println("Inserta el Codigo Postal"+ columnas[4] +": ");
		codi_postal = teclado.next();
		codi_postal = codi_postal != ""?codi_postal:columnas[0];

		System.out.println("Inserta el Población"+ columnas[5] +": ");
		poblacio = teclado.next();
		poblacio = poblacio != ""?poblacio:columnas[0];



		selectStmt.execute("UPDATE alumnes SET nom = '" + nom + "', data_naixement = '" + data_naixement + "', adreca_postal = '" + adreca_postal +"',"
				+ " codi_postal = '" + codi_postal + "', poblacio = '" + poblacio + "', WHERE id = " + id);

	}

	public static String[] list(Statement selectStmt) throws SQLException {
		ResultSet rs = selectStmt.executeQuery("SELECT id,nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio  FROM alumnes");
		String[] columnas = new String[6];
		while(rs.next())
		{
			System.out.println("-ID: " + rs.getString("id"));
			System.out.print("---Nombre: " + rs.getString("nom") + ", ");    
			System.out.print("DNI: " + rs.getString("dni")+ ", ");    
			System.out.print("Data de naixement: " + rs.getString("data_naixement")+ ", ");    
			System.out.print("Codigo Postal: " + rs.getString("adreca_postal")+ ", ");    
			System.out.print("Sexo: " + rs.getString("sexe")+ ", ");    
			System.out.print("Codigo Postal: " + rs.getString("codi_postal")+ ", ");    
			System.out.println("Poblacion: " + rs.getString("poblacio"));    
			System.out.println();
			columnas[0] = rs.getString("nom");
			columnas[1] = rs.getString("data_naixement");
			columnas[2] = rs.getString("adreca_postal");
			columnas[3] = rs.getString("sexe");
			columnas[4] = rs.getString("codi_postal");
			columnas[5] = rs.getString("poblacio");
		}
		return columnas;
	}

	public static void listOne(Statement selectStmt) throws SQLException {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Inserta la ID para buscar al alumno: ");
		int id = teclado.nextInt();

		ResultSet rs = selectStmt.executeQuery("SELECT id, nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio  FROM alumnes WHERE id = " + id);

		while(rs.next()) {
			System.out.println("-ID: " + rs.getString("id"));
			System.out.print("---Nombre: " + rs.getString("nom") + ", "); 
			System.out.print("DNI: " + rs.getString("dni")+ ", ");
			System.out.print("Data de naixement: " + rs.getString("data_naixement")+ ", ");
			System.out.print("Codigo Postal: " + rs.getString("adreca_postal")+ ", ");
			System.out.print("Sexo: " + rs.getString("sexe")+ ", ");
			System.out.print("Codigo Postal: " + rs.getString("codi_postal")+ ", ");
			System.out.println("Poblacion: " + rs.getString("poblacio"));
			System.out.println();
		}			
	}
}
