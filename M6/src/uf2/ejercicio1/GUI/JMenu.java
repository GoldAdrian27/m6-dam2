package uf2.ejercicio1.GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import uf2.ejercicio1.MainApp;

import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;

public class JMenu extends JFrame {

	private JPanel contentPane;
	private JTextField tfShowOneId;
	private JTextField tfDeleteId;
	private JTextField tfUpdateNom;
	private JTextField tfUpdateDNI;
	private JTextField tfUpdateCodiPostal;
	private JTextField tfUpdateSexe;
	private JTextField tfUpdateDataNaix;
	private JTextField tfUpdatePoblacio;
	private JTextField tfInsertNom;
	private JTextField tfInsertDNI;
	private JTextField tfInsertData;
	private JTextField tfInsertAdreca;
	private JTextField tfInsertSexe;
	private JTextField tfInsertCodi;
	private JTextField tfInsertPoblacio;
	private JTextField tfUpdateAdreca;
	private JComboBox cbUpdateId;
	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {

		Connection con = null;
		Driver driver;
		String url = "jdbc:mysql://127.0.0.1:3306/m6_uf2_1";
		String usuari = "root";
		String password = "";
		Scanner teclado = new Scanner(System.in);
		boolean run = true;


		System.out.println("provaDeConexio()");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			con = DriverManager.getConnection(url, usuari, password);
			System.out.println("Connexi� realitzada usant DriverManager");
		} catch (Exception e) {
			System.out.println("Error " + e.getMessage());
		}
		Statement selectStmt = con.createStatement();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JMenu frame = new JMenu(selectStmt);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JMenu(Statement selectStmt) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 671, 291);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton bList = new JButton("List");

		bList.setBounds(24, 39, 89, 23);
		contentPane.add(bList);

		JButton bInsertar = new JButton("Insert");
		bInsertar.setBounds(24, 73, 89, 23);
		contentPane.add(bInsertar);

		JButton bActualizar = new JButton("Update");
		bActualizar.setBounds(24, 107, 89, 23);
		contentPane.add(bActualizar);

		JButton bEliminar = new JButton("Delete");
		bEliminar.setBounds(24, 141, 89, 23);
		contentPane.add(bEliminar);

		JButton bShowOne = new JButton("ShowOne");
		bShowOne.setBounds(24, 175, 89, 23);
		contentPane.add(bShowOne);

		JLayeredPane layeredPane = new JLayeredPane();
		layeredPane.setBounds(190, 11, 455, 230);
		contentPane.add(layeredPane);

		JPanel pUpdate = new JPanel();
		pUpdate.setBounds(0, 0, 455, 230);
		layeredPane.add(pUpdate);
		pUpdate.setLayout(null);
		pUpdate.setVisible(false);

		JLabel lbUpdate = new JLabel("Update");
		lbUpdate.setHorizontalAlignment(SwingConstants.CENTER);
		lbUpdate.setBounds(0, 0, 455, 14);
		pUpdate.add(lbUpdate);

		JLabel lblUpdateId = new JLabel("Id");
		lblUpdateId.setBounds(21, 37, 46, 14);
		pUpdate.add(lblUpdateId);

		tfUpdateNom = new JTextField();
		tfUpdateNom.setBounds(120, 62, 86, 20);
		pUpdate.add(tfUpdateNom);
		tfUpdateNom.setColumns(10);

		tfUpdateDNI = new JTextField();
		tfUpdateDNI.setBounds(120, 93, 86, 20);
		pUpdate.add(tfUpdateDNI);
		tfUpdateDNI.setColumns(10);

		tfUpdateCodiPostal = new JTextField();
		tfUpdateCodiPostal.setBounds(356, 90, 86, 20);
		pUpdate.add(tfUpdateCodiPostal);
		tfUpdateCodiPostal.setColumns(10);

		tfUpdateSexe = new JTextField();
		tfUpdateSexe.setBounds(356, 62, 86, 20);
		pUpdate.add(tfUpdateSexe);
		tfUpdateSexe.setColumns(10);

		tfUpdateDataNaix = new JTextField();
		tfUpdateDataNaix.setBounds(120, 121, 86, 20);
		pUpdate.add(tfUpdateDataNaix);
		tfUpdateDataNaix.setColumns(10);

		JButton btnUpdate = new JButton("Update");

		btnUpdate.setBounds(356, 196, 89, 23);
		pUpdate.add(btnUpdate);

		JLabel lblUpdateNom = new JLabel("Nom");
		lblUpdateNom.setBounds(21, 62, 46, 14);
		pUpdate.add(lblUpdateNom);

		JLabel lblUpdateDNI = new JLabel("DNI");
		lblUpdateDNI.setBounds(21, 93, 46, 14);
		pUpdate.add(lblUpdateDNI);

		JLabel lblUpdateDateNaix = new JLabel("Data Naixement");
		lblUpdateDateNaix.setBounds(21, 124, 99, 14);
		pUpdate.add(lblUpdateDateNaix);

		JLabel lblUpdateSexe = new JLabel("Sexe");
		lblUpdateSexe.setBounds(260, 65, 86, 14);
		pUpdate.add(lblUpdateSexe);

		JLabel lblUpdateCodi = new JLabel("Codi Postal");
		lblUpdateCodi.setBounds(260, 93, 86, 14);
		pUpdate.add(lblUpdateCodi);

		JLabel lblUpdatePoblacio = new JLabel("Poblaci\u00F3");
		lblUpdatePoblacio.setBounds(260, 124, 86, 14);
		pUpdate.add(lblUpdatePoblacio);

		tfUpdatePoblacio = new JTextField();
		tfUpdatePoblacio.setBounds(356, 121, 86, 20);
		pUpdate.add(tfUpdatePoblacio);
		tfUpdatePoblacio.setColumns(10);

		tfUpdateAdreca = new JTextField();
		tfUpdateAdreca.setBounds(356, 34, 86, 20);
		pUpdate.add(tfUpdateAdreca);
		tfUpdateAdreca.setColumns(10);

		JLabel lblUpdateAdreca = new JLabel("Adre\u00E7a Postal");
		lblUpdateAdreca.setBounds(260, 37, 86, 14);
		pUpdate.add(lblUpdateAdreca);

		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				update(selectStmt);
			}
		});

		JPanel pInsert = new JPanel();
		pInsert.setBounds(0, 0, 455, 230);
		layeredPane.add(pInsert);
		pInsert.setLayout(null);
		pInsert.setVisible(false);

		JLabel lbInsert = new JLabel("Insert");
		lbInsert.setHorizontalAlignment(SwingConstants.CENTER);
		lbInsert.setBounds(0, 0, 455, 14);
		pInsert.add(lbInsert);

		JLabel lblInsertDNI = new JLabel("DNI");
		lblInsertDNI.setBounds(10, 77, 86, 14);
		pInsert.add(lblInsertDNI);

		JLabel lblInsertAdreca = new JLabel("Adre\u00E7a Postal");
		lblInsertAdreca.setBounds(10, 153, 86, 14);
		pInsert.add(lblInsertAdreca);

		JLabel lblInsertDataNaix = new JLabel("Data Naixement");
		lblInsertDataNaix.setBounds(10, 102, 114, 14);
		pInsert.add(lblInsertDataNaix);

		JLabel lblInsertSexe = new JLabel("Sexe");
		lblInsertSexe.setBounds(282, 52, 61, 14);
		pInsert.add(lblInsertSexe);

		JLabel lblInsertCodiPostal = new JLabel("Codi Postal");
		lblInsertCodiPostal.setBounds(282, 77, 61, 14);
		pInsert.add(lblInsertCodiPostal);

		JLabel lblInsertPoblacio = new JLabel("Poblaci\u00F3");
		lblInsertPoblacio.setBounds(282, 102, 61, 14);
		pInsert.add(lblInsertPoblacio);

		JLabel lblInsertNom = new JLabel("Nom");
		lblInsertNom.setBounds(10, 52, 86, 14);
		pInsert.add(lblInsertNom);

		tfInsertNom = new JTextField();
		tfInsertNom.setBounds(134, 52, 86, 20);
		pInsert.add(tfInsertNom);
		tfInsertNom.setColumns(10);

		tfInsertDNI = new JTextField();
		tfInsertDNI.setBounds(134, 77, 86, 20);
		pInsert.add(tfInsertDNI);
		tfInsertDNI.setColumns(10);

		tfInsertData = new JTextField();
		tfInsertData.setBounds(134, 102, 86, 20);
		pInsert.add(tfInsertData);
		tfInsertData.setColumns(10);

		tfInsertAdreca = new JTextField();
		tfInsertAdreca.setBounds(134, 153, 86, 20);
		pInsert.add(tfInsertAdreca);
		tfInsertAdreca.setColumns(10);

		tfInsertSexe = new JTextField();
		tfInsertSexe.setBounds(353, 49, 86, 20);
		pInsert.add(tfInsertSexe);
		tfInsertSexe.setColumns(10);

		tfInsertCodi = new JTextField();
		tfInsertCodi.setBounds(353, 74, 86, 20);
		pInsert.add(tfInsertCodi);
		tfInsertCodi.setColumns(10);

		tfInsertPoblacio = new JTextField();
		tfInsertPoblacio.setBounds(353, 99, 86, 20);
		pInsert.add(tfInsertPoblacio);
		tfInsertPoblacio.setColumns(10);

		JButton btnInsert = new JButton("Insert");
		btnInsert.setBounds(179, 196, 89, 23);
		pInsert.add(btnInsert);

		JLabel lblNewLabel_1 = new JLabel("yyyy-mm-dd");
		lblNewLabel_1.setBounds(10, 115, 114, 14);
		pInsert.add(lblNewLabel_1);
		//Insert
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					insertar(selectStmt);
				} catch (SQLException e) {
					System.out.println("Ha habido un problema al insertar vuelvelo a intentar\nError: " + e );				}
			}
		});



		JPanel pShowOne = new JPanel();
		pShowOne.setBounds(0, 0, 455, 230);
		layeredPane.add(pShowOne);
		pShowOne.setLayout(null);
		pShowOne.setVisible(false);

		JLabel lbShowOne = new JLabel("Show one");
		lbShowOne.setHorizontalAlignment(SwingConstants.CENTER);
		lbShowOne.setBounds(0, 0, 455, 14);
		pShowOne.add(lbShowOne);

		tfShowOneId = new JTextField();
		tfShowOneId.setBounds(72, 56, 86, 20);
		pShowOne.add(tfShowOneId);
		tfShowOneId.setColumns(10);

		JLabel lbSearchOne = new JLabel("Id");
		lbSearchOne.setBounds(43, 59, 19, 14);
		pShowOne.add(lbSearchOne);

		JButton btnSearchOne = new JButton("Search");
		btnSearchOne.setBounds(322, 55, 89, 23);
		pShowOne.add(btnSearchOne);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 108, 435, 111);
		pShowOne.add(scrollPane_2);

		JTextPane tpShowOne = new JTextPane();
		scrollPane_2.setViewportView(tpShowOne);

		JPanel pDelete = new JPanel();
		pDelete.setBounds(0, 0, 455, 230);
		layeredPane.add(pDelete);
		pDelete.setLayout(null);
		pDelete.setVisible(false);

		JLabel lbDelete = new JLabel("Delete");
		lbDelete.setHorizontalAlignment(SwingConstants.CENTER);
		lbDelete.setBounds(0, 0, 455, 14);
		pDelete.add(lbDelete);

		JLabel lblNewLabel = new JLabel("id");
		lblNewLabel.setBounds(166, 100, 46, 14);
		pDelete.add(lblNewLabel);

		JButton btnNewButton = new JButton("Delete");

		btnNewButton.setBounds(182, 175, 89, 23);
		pDelete.add(btnNewButton);

		tfDeleteId = new JTextField();
		tfDeleteId.setBounds(241, 97, 86, 20);
		pDelete.add(tfDeleteId);
		tfDeleteId.setColumns(10);

		JPanel pList = new JPanel();
		pList.setBounds(0, 0, 455, 230);
		layeredPane.add(pList);
		pList.setLayout(null);
		pList.setVisible(false);

		JLabel lbList = new JLabel("List");
		lbList.setHorizontalAlignment(SwingConstants.CENTER);
		lbList.setBounds(0, 0, 455, 14);
		pList.add(lbList);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 50, 435, 169);
		pList.add(scrollPane);

		JTextPane tpList = new JTextPane();
		tpList.setEditable(false);
		scrollPane.setViewportView(tpList);

		//Listar Evento
		bList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pList.setVisible(true);
				pInsert.setVisible(false);
				pUpdate.setVisible(false);
				pDelete.setVisible(false);
				pShowOne.setVisible(false);

				try {
					StringBuilder liststrB = new StringBuilder();
					ArrayList<String> list = list(selectStmt);
					for (int i = 0; i < list.size(); i++) {
						liststrB.append(list.get(i) + " ");
					}
					tpList.setText(liststrB.toString());

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		bActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pList.setVisible(false);
				pInsert.setVisible(false);
				pUpdate.setVisible(true);
				pDelete.setVisible(false);
				pShowOne.setVisible(false);

				try {
					ArrayList<String> row = getIds(selectStmt);
					cbUpdateId = new JComboBox(row.toArray());
					cbUpdateId.setBounds(120, 34, 86, 20);
					pUpdate.add(cbUpdateId);
					cbUpdateId.addActionListener (new ActionListener () {
						public void actionPerformed(ActionEvent e) {
							try {
								HashMap<String, String> row = showOneHM(selectStmt, cbUpdateId.getSelectedItem().toString());

								tfUpdateNom.setText(row.get("Nombre"));
								tfUpdateDNI.setText(row.get("DNI"));
								tfUpdateDataNaix.setText(row.get("Data de naixement"));
								tfUpdateAdreca.setText(row.get("Adre�a Postal"));
								tfUpdateSexe.setText(row.get("Sexo"));
								tfUpdateCodiPostal.setText(row.get("Codigo Postal"));
								tfUpdatePoblacio.setText(row.get("Poblacion"));

							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		bEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pList.setVisible(false);
				pInsert.setVisible(false);
				pUpdate.setVisible(false);
				pDelete.setVisible(true);
				pShowOne.setVisible(false);
			}
		});
		bInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pList.setVisible(false);
				pInsert.setVisible(true);
				pUpdate.setVisible(false);
				pDelete.setVisible(false);
				pShowOne.setVisible(false);
			}
		});
		bShowOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pList.setVisible(false);
				pInsert.setVisible(false);
				pUpdate.setVisible(false);
				pDelete.setVisible(false);
				pShowOne.setVisible(true);
			}
		});
		//Eliminar
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id;
				try {
					id = Integer.parseInt(tfDeleteId.getText());
					delete(selectStmt, id);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Introduce un numero!");
				}
			}
		});
		//Show One 
		btnSearchOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int id =  Integer.parseInt(tfShowOneId.getText());
					String row = showOne(selectStmt, id);
					tpShowOne.setText(row);
				}catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Introduce un numero!");
				}
			}
		});

	}

	public static ArrayList<String> list(Statement selectStmt) throws SQLException {
		ResultSet rs = selectStmt.executeQuery("SELECT id,nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio  FROM alumnes");
		ArrayList<String> row = new ArrayList<String>();
		while(rs.next()){
			row.add("-ID: " + rs.getString("id") + 
					", Nombre: " + rs.getString("nom") + ", " + 
					"DNI: " + rs.getString("dni")+ ", " + 
					"Data de naixement: " + rs.getString("data_naixement")+ ", " + 
					"Codigo Postal: " + rs.getString("adreca_postal")+ ", " + 
					"Sexo: " + rs.getString("sexe")+ ", " + 
					"Codigo Postal: " + rs.getString("codi_postal")+ ", " + 
					"Poblacion: " + rs.getString("poblacio") + "\n\n");
		}
		return row;
	}

	public void delete(Statement selectStmt , int id) throws SQLException {
		JFrame frame = new JFrame("Delete");
		if (showOne(selectStmt, id).equals("No existe")) {
			JOptionPane.showMessageDialog(null, "Lo sentimos la id introducida no existe");
			return;
		}
		int result = JOptionPane.showConfirmDialog(frame,"�Estas seguro que quieres eliminarlo?", "Eliminar",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE);

		if(result == 0) {
			try {
				selectStmt.execute("DELETE FROM alumnes where id = " + id);
				JOptionPane.showMessageDialog(null, "Eliminado correctametne");

			} catch (Exception e) {
				System.out.println(e);			}
		}else {
			JOptionPane.showMessageDialog(null, "No se ha eliminado");
		}
	}

	public void insertar(Statement selectStmt) throws SQLException {
		Scanner teclado = new Scanner(System.in);
		String nom;
		String dni;
		String data_naixement;
		String adreca_postal;
		String sexe;
		String codi_postal;
		String poblacio;

		nom = tfInsertNom.getText();
		dni = tfInsertDNI.getText();
		data_naixement = tfInsertData.getText();
		adreca_postal = tfInsertAdreca.getText();
		sexe = tfInsertSexe.getText();
		codi_postal = tfInsertCodi.getText();
		poblacio = tfInsertPoblacio.getText();
		if ( nom.equals("") || dni.equals("") || data_naixement.equals("") || adreca_postal.equals("") || sexe.equals("") || codi_postal.equals("") || poblacio.equals("")) {
			JOptionPane.showMessageDialog(null, "Rellena todos los campos");
			return;
		}

		try {
			selectStmt.execute("INSERT INTO alumnes (nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio) VALUES ('" + nom + "', '" + dni + "', '" + data_naixement + "', '" + adreca_postal + "', '" + sexe + "', '" + codi_postal + "', '" + poblacio + "')");
			JOptionPane.showMessageDialog(null, "Introducido correctamente");

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Lo sentimos ha habido un problema al insertar");
		}

	}

	public String showOne (Statement selectStmt, int id) throws SQLException {
		String row = "No existe";

		try {
			ResultSet rs = selectStmt.executeQuery("SELECT id, nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio  FROM alumnes WHERE id = " + id);
			while(rs.next()){
				row = ("-ID: " + rs.getString("id") + 
						", Nombre: " + rs.getString("nom") + ", " + 
						"DNI: " + rs.getString("dni")+ ", " + 
						"Data de naixement: " + rs.getString("data_naixement")+ ", " + 
						"Codigo Postal: " + rs.getString("adreca_postal")+ ", " + 
						"Sexo: " + rs.getString("sexe")+ ", " + 
						"Codigo Postal: " + rs.getString("codi_postal")+ ", " + 
						"Poblacion: " + rs.getString("poblacio") + "\n\n");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Lo sentimos ha habido un problema al buscar la fila");
		}
		return row;
	}

	public ArrayList<String> getIds (Statement selectStmt) throws SQLException {

		ArrayList<String> row = new ArrayList<String>();
		try {
			ResultSet rs = selectStmt.executeQuery("SELECT id FROM alumnes ");
			while(rs.next()){
				row.add(rs.getString("id"));
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Lo sentimos ha habido un problema al buscar la fila");
		}
		return row;
	}

	public HashMap<String, String> showOneHM (Statement selectStmt, String id) throws SQLException {
		HashMap<String, String> row2 = new HashMap<String, String>();
		try {
			ResultSet rs = selectStmt.executeQuery("SELECT id, nom, dni, data_naixement, adreca_postal, sexe, codi_postal, poblacio  FROM alumnes WHERE id = " + id);
			while(rs.next()){
				row2.put("ID", rs.getString("id"));
				row2.put("Nombre", rs.getString("nom"));
				row2.put("DNI", rs.getString("dni"));
				row2.put("Data de naixement", rs.getString("data_naixement"));
				row2.put("Adre�a Postal", rs.getString("adreca_postal"));
				row2.put("Sexo", rs.getString("sexe"));
				row2.put("Codigo Postal", rs.getString("codi_postal"));
				row2.put("Poblacion", rs.getString("poblacio"));
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Lo sentimos ha habido un problema al buscar la fila");
		}
		return row2;
	}

	public void update(Statement selectStmt) {
		String id;
		String nom;
		String dni;
		String data_naixement;
		String adreca_postal;
		String sexe;
		String codi_postal;
		String poblacio;

		id = cbUpdateId.getSelectedItem().toString().trim();
		nom = tfUpdateNom.getText().trim();
		dni = tfUpdateDNI.getText().trim();
		data_naixement = tfUpdateDataNaix.getText().trim();
		adreca_postal = tfUpdateAdreca.getText().trim();
		sexe = tfUpdateSexe.getText().trim();
		codi_postal = tfUpdateCodiPostal.getText().trim();
		poblacio = tfUpdatePoblacio.getText().trim();

		System.out.print(nom instanceof String);
		if (id.equals("") || nom.equals("") || dni.equals("") || data_naixement.equals("") || adreca_postal.equals("") || sexe.equals("") || codi_postal.equals("") || poblacio.equals("")) {
			JOptionPane.showMessageDialog(null, "Rellena todos los campos");
			return;
		}
		System.out.println(id + " " + nom + " " + dni + " " + data_naixement+ " " + adreca_postal+ " " + sexe+ " " + codi_postal+ " " + poblacio);

		try {
			selectStmt.execute("UPDATE alumnes SET nom = '" + nom + "', dni = '" + dni + "', data_naixement = '" + data_naixement + "', adreca_postal = '" + adreca_postal +"',"
					+ " codi_postal = '" + codi_postal + "', poblacio = '" + poblacio + "' WHERE id = " + id);
			JOptionPane.showMessageDialog(null, "Actualizado correctamente");


		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Lo sentimos ha habido un problema al actualizar la fila");
			System.out.println(e);
		}

	}
}		
