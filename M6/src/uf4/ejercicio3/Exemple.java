package uf4.ejercicio3;

import java.util.ArrayList;
import java.util.Scanner;

import javalibrary1.*;

public class Exemple {
	public static void main(String args[]) {
		Scanner teclado = new Scanner(System.in);
		String urldb = "jdbc:mysql://localhost/m6_uf4_3";
		String usuari = "root";
		String contrasena = "";
		String driver = "com.mysql.jdbc.Driver";

		BaseDades bd = new BaseDades(urldb, usuari, contrasena, driver);
		bd.setCrearConnexio();

		if(bd.getCrearConnexio()) {
			int num = 0;
			do {
				System.out.println(
						"1 - LLISTA INICIAL DE PRODUCTES"
								+ "\n2 - CREA VENDA"
								+ "\n3 - LLISTA DE PRODUCTES DESPR�S DE CREAR VENDA"
								+ "\n4 - LLISTA DE VENDES"
								+ "\n5 - LLISTA DE COMANDES"
								+ "\n6 - CREA PRODUCTE");

				num = teclado.nextInt();
				switch (num) {
				case 1:
					System.out.println("=======================");
					System.out.println("LLISTA INICIAL DE PRODUCTES");
					VeureProductes(bd);
					break;
				case 2:
					System.out.println("=================");
					System.out.println("ES CREA VENDA");
					System.out.println("\nLLISTA DE PRODUCTES ");
					VeureProductes(bd);
					System.out.println("ESCRIU ID DE LA VENDA");
					int id = teclado.nextInt();
					System.out.println("ESCRIU LA QUANTITAT DE LA VENDA");
					int quantitat = teclado.nextInt();
					CrearVenda(bd, id, quantitat);
					break;
				case 3:
					System.out.println("====================");
					System.out.println("LLISTA DE PRODUCTES DESPR�S DE CREAR VENDA");
					VeureProductes(bd);
					break;
				case 4:
					System.out.println("===========================");
					System.out.println("LLISTA DE VENDES");
					VeureVendes(bd);
					break;
				case 5:
					System.out.println("=======================");
					System.out.println("LLISTA DE COMANDES");
					VeureComandes(bd); 
					break;
					
				case 6:
					System.out.println("=================");
					System.out.println("CREAR PRODUCTE");
					System.out.println("\nLLISTA DE PRODUCTES ");
					VeureProductes(bd);
					System.out.println("ESCRIU LA DESCRIPCIO DEL PRODUCTE");
					teclado.nextLine();
					String descripcio = teclado.nextLine();
					System.out.println("ESCRIU L'STOCK ACTUAL DEL PRODUCTE");
					int stockActual = teclado.nextInt();
					System.out.println("ESCRIU L'STOCK MINIM DEL PRODUCTE");
					int stockMinim = teclado.nextInt();
					System.out.println("ESCRIU EL PVP DEL PRODUCTE");
					int pvp = teclado.nextInt();
					CrearProducte(bd, descripcio, stockActual, stockMinim, pvp);
					break;
					
				default:
					break;
				}
			}while(num != -1);



		}else {
			System.out.println("No connectat");
			bd.tancarConnexio();
		}
	}
	private static void VeureProductes (BaseDades bd) {
		ArrayList<Producte> llista = new ArrayList <Producte>();
		llista = bd.consultaPro("SELECT * FROM PRODUCTE");
		if(llista != null) {
			for (int i = 0; i < llista.size(); i++) {
				Producte p = (Producte) llista.get(i);
				System.out.println("ID=>" + p.getIdproducte() + ": "
						+ p.getDescripcio() + "* Estoc M�nim: "
						+ p.getStockminim());
			}
		}
	}

	private static void CrearVenda (BaseDades bd, int idproducte, int quantitat) {
		Producte prod = bd.consultarProducte(idproducte);
		java.sql.Date dataActual = getCurrentDate();
		if (prod != null) {
			if (bd.actualizarStock (prod, quantitat, dataActual)>0 ) {
				String taula = "VENDES";
				int idvenda = bd.obtenirUltimID(taula);
				Venda ven = new Venda (idvenda, prod.getIdproducte(), dataActual, quantitat);

				if(bd.inserirVenda(ven)>0) {
					System.out.println("VENDA INSERIDA");
				} else {
					System.out.println("NO ES POT FER LA VENDA, NO HI HA ESTOC...");
				}
			}else {
				System.out.println("NO HI HA EL PRODUCTE");
			}
		}
	}

	private static void CrearProducte (BaseDades bd, String descripcio, int stockActual , int stockMinim, float pvp) {

			String taula = "PRODUCTE";
			int idproducte = bd.obtenirUltimID(taula);
			Producte produc = new Producte (idproducte, descripcio, stockActual, stockMinim, pvp);

			if(bd.inserirProducte(produc)>0) {
				System.out.println("PRODUCTE INSERIT");
			} else {
				System.out.println("NO ES POT AFEGIR EL PRODUCTE");
			}
	}

	private static void VeureComandes (BaseDades bd) {
		ArrayList <Comanda> llista = new ArrayList<Comanda>();
		llista = bd.consultaCom("SELECT * FROM COMANDES");
		if (llista != null) {
			for (int i = 0; i < llista.size(); i++) {
				Comanda c = (Comanda) llista.get(i);
				Producte prod = bd.consultarProducte(c.getIdproducte());
				System.out.println("ID Comanda =>" + c.getNumcomanda() 
				+ "* Producte: " + prod.getDescripcio() + "* Quantitat: "
				+ c.getQuantitat() + "* Data: " + c.getData());
			}
		}
	}
	private static void VeureVendes (BaseDades bd) {
		ArrayList <Venda> llista = new ArrayList <Venda>();
		llista = bd.consultaVen("SELECT * FROM VENDES");
		if(llista != null) {
			for (int i = 0; i < llista.size(); i++) {
				Venda p = (Venda) llista.get(i);
				Producte prod = bd.consultarProducte(p.getIdproducte());
				System.out.println("ID Venda => " + p.getNumvenda() + "* Producte: "
						+ prod.getDescripcio() + "* Quantitat: " + p.getQuantitat()
						+ "* Data: " + p.getDatavenda());
			}
		}
	}

	private static java.sql.Date getCurrentDate(){
		java.util.Date avui = new java.util.Date();
		return new java.sql.Date(avui.getTime());
	}
}
