package uf4.ejercicio1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.Date;

public class Comanda implements Serializable, PropertyChangeListener {
	
	private int numcomanda;
	private int idproducte;
	private Date data;
	private int quantitat;
	private boolean demana;
	
	public Comanda() {}

	
	
	public Comanda(int numcomanda, int idproducte, Date data, int quantitat) {
		this.numcomanda = numcomanda;
		this.idproducte = idproducte;
		this.data = data;
		this.quantitat = quantitat;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		System.out.println("Valor anterior: " + evt.getOldValue());
		System.out.println("Valor actual: " + evt.getNewValue());
		setDemana(true);
	}

	public int getNumcomanda() {
		return numcomanda;
	}

	public void setNumcomanda(int numcomanda) {
		this.numcomanda = numcomanda;
	}

	public int getIdproducte() {
		return idproducte;
	}

	public void setIdproducte(int idproducte) {
		this.idproducte = idproducte;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public boolean isDemana() {
		return demana;
	}

	public void setDemana(boolean demana) {
		this.demana = demana;
	}
}