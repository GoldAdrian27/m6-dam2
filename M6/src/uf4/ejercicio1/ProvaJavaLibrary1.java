
package uf4.ejercicio1;

import javalibrary1.Comanda;
import javalibrary1.Producte;

public class ProvaJavaLibrary1 {

	public static void main(String[] args) {
		Producte producte = new Producte(1, "Portable MSI USB 3.0", 2, 4, 50);
		Producte producte2 = new Producte(2, "GTX 1080 ti 12GB", 4, 5, 50);
		Producte producte3 = new Producte(3, "HyperX 8GB RAM", 12, 10, 50);
		Producte producte4 = new Producte(4, "SSD 2T 3000mb/s", 3, 20, 50);
		Comanda comanda = new Comanda();
		Comanda comanda2 = new Comanda();
		Comanda comanda3 = new Comanda();
		Comanda comanda4 = new Comanda();
		
		producte.addPropertyChangeListener(comanda);
		producte2.addPropertyChangeListener(comanda2);
		producte3.addPropertyChangeListener(comanda3);
		producte4.addPropertyChangeListener(comanda4);
		
		System.out.println(producte.getDescripcio());
		producte.setStockactual(2);
		
		System.out.println("\n" + producte2.getDescripcio());
		producte2.setStockactual(0);
		
		System.out.println("\n" + producte3.getDescripcio());
		producte3.setStockactual(4);

		
		System.out.println("\n" + producte4.getDescripcio());
		producte4.setStockactual(8);
		
		System.out.println();
		if(comanda.isDemana()) {
			System.out.println("Fer comanda en producte: " + producte.getDescripcio());
		}else {
			System.out.println("No �s necessari fer la comanda en producte: " 
							+ producte.getDescripcio());
		}
		System.out.println("Stock minim: " + producte.getStockminim());

		if(comanda2.isDemana()) {
			System.out.println("Fer comanda en producte: " + producte2.getDescripcio());
		}else {
			System.out.println("No �s necessari fer la comanda en producte: " 
							+ producte2.getDescripcio());
		}	
		System.out.println("Stock minim: " + producte2.getStockminim());

		if(comanda3.isDemana()) {
			System.out.println("Fer comanda en producte: " + producte3.getDescripcio());
		}else {
			System.out.println("No �s necessari fer la comanda en producte: " 
							+ producte3.getDescripcio());
		}	
		System.out.println("Stock minim: " + producte3.getStockminim());

		if(comanda4.isDemana()) {
			System.out.println("Fer comanda en producte: " + producte4.getDescripcio());
		}else {
			System.out.println("No �s necessari fer la comanda en producte: " 
							+ producte4.getDescripcio());
		}	
		System.out.println("Stock minim: " + producte4.getStockminim());

	}	
	
}	
	