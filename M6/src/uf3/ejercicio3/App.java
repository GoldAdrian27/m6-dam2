package uf3.ejercicio3;

import org.xmldb.api.*;
import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;

public class App {

	public static void main(String[] args) throws XMLDBException {

		String driver = "org.exist.xmldb.DatabaseImpl";
		Collection col = null;
		String URI = "xmldb:exist://localhost:8080/exist/xmlrcp/db/data_base_name_";
		String usu = "admin";
		String usuPass = "";

		try {
			Class cl = Class.forName(driver);
			Database database = (Database) cl.newInstance();
			DatabaseManager.deregisterDatabase(database);

		} catch (Exception e) {
			System.out.println("error");		}
		col = DatabaseManager.getCollection(URI, usu, usuPass);
		if(col == null) {
			System.out.println("COL�LECCIO NO EXISTEIX");
		}
		XPathQueryService servei = (XPathQueryService) col.getService("XPathQueryService", "1.0");
		ResourceSet result = servei.query("for $em in");
		
		ResourceIterator i;
		i = result.getIterator();
		if(!i.hasMoreResources()) {
			System.out.println("LA CONSULTA NO TORNA RES");
		}
		while (i.hasMoreResources()) {
			Resource r = i.nextResource();
			System.out.println((String) r.getContent());
			
		}
		col.close();

	}
			
}
