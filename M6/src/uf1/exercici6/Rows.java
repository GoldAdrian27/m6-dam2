package uf1.exercici6;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Rows {

	private Row[] row;

	public Row[] getRow() {
		return row;
	}

	public void setRow(Row[] row) {
		this.row = row;
	}
	
	
}
