package uf1.exercici6;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "row")
public class Row {
	
	private int id;
	private String _uuid;
	private String _position;
	private String _address;
	private String data;
	private String nous_casos_diaris_confirmats;
	private String defuncions_diaries;
	private String total_de_casos_confirmats;
	private String total_de_defuncions;
	
	@XmlAttribute(name="_id")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String get_uuid() {
		return _uuid;
	}
	
	@XmlAttribute(name="_uuid")
	public void set_uuid(String _uuid) {
		this._uuid = _uuid;
	}
	public String get_position() {
		return _position;
	}
	
	@XmlAttribute(name="_position")
	public void set_position(String _position) {
		this._position = _position;
	}
	public String get_address() {
		return _address;
	}
	
	@XmlAttribute(name="_address")
	public void set_address(String _address) {
		this._address = _address;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getNous_casos_diaris_confirmats() {
		return nous_casos_diaris_confirmats;
	}
	public void setNous_casos_diaris_confirmats(String nous_casos_diaris_confirmats) {
		this.nous_casos_diaris_confirmats = nous_casos_diaris_confirmats;
	}
	public String getDefuncions_diaries() {
		return defuncions_diaries;
	}
	public void setDefuncions_diaries(String defuncions_diaries) {
		this.defuncions_diaries = defuncions_diaries;
	}
	public String getTotal_de_casos_confirmats() {
		return total_de_casos_confirmats;
	}
	public void setTotal_de_casos_confirmats(String total_de_casos_confirmats) {
		this.total_de_casos_confirmats = total_de_casos_confirmats;
	}
	public String getTotal_de_defuncions() {
		return total_de_defuncions;
	}
	public void setTotal_de_defuncions(String total_de_defuncions) {
		this.total_de_defuncions = total_de_defuncions;
	}
	
	
}
