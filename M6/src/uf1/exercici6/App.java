package uf1.exercici6;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;



public class App {
	private static final String ROWS_XML_FILE = "./src/uf1/exercici6/arxiuxml.xml";

	public static void main(String[] args) throws JAXBException, IOException {
		JAXBContext context = JAXBContext.newInstance(Rows.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		
		Rows rows = ompleRows();
		
		//Mostrem el document XML generat por la sortida estandard
		marshaller.marshal(rows, System.out);
		
		FileOutputStream fos = new FileOutputStream(ROWS_XML_FILE);
		//guardem l'objecte serializat en un document XML
		marshaller.marshal(rows, fos);
		fos.close();
		
		Unmarshaller unmarshaller = context.createUnmarshaller();
		//Deserialitzem a partir de un document XML
		Rows rowsAux = (Rows) unmarshaller.unmarshal(new File(ROWS_XML_FILE));
		System.out.println("********* Rows cargado desde el archivo xml***************");
		//Mostrem l'objeto Java obtingut
		marshaller.marshal(rowsAux, System.out);
	}
	
	private static Rows ompleRows(){
		
		String[] _uuids = {"2312123", "3213123213", "32132131"};
        String[] _position = {"A4", "B5", "F8"};
        String[] _address = {"calle valenti", "www.google.es", "hola@ejemplo.com"};
        String[] data = {"22/05/2020", "12/10/2020", "30/09/2020"};
        String[] nous_casos_diaris_confirmats = {"675", "324", "789"};
        String[] defuncions_diaries = {"23", "43", "32"};
        String[] total_de_casos_confirmats = {"321", "43", "23"};
        String[] total_de_defuncions = {"321", "32", "343"};
		Row[] ArrayRows = new Row[3];
		
		//Afegim tots els valors als atributs del objecte
		for(int i=0; i<3; i++){
			ArrayRows[i] = new Row();
			ArrayRows[i].set_uuid(_uuids[i]);
			ArrayRows[i].set_position(_position[i]);
			ArrayRows[i].set_address(_address[i]);
			ArrayRows[i].setData(data[i]);
			ArrayRows[i].setNous_casos_diaris_confirmats(nous_casos_diaris_confirmats[i]);
			ArrayRows[i].setDefuncions_diaries(defuncions_diaries[i]);
			ArrayRows[i].setTotal_de_casos_confirmats(total_de_casos_confirmats[i]);
			ArrayRows[i].setTotal_de_defuncions(total_de_defuncions[i]);
			
			ArrayRows[i].setId(i);			
		}
		
		Rows rows = new Rows();
		rows.setRow(ArrayRows);
		
		return rows;
	}

}
