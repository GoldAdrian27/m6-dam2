package uf1.exercici2;

import java.io.Serializable;

public class Cotxe implements Serializable{

	private String marca;
	private String model;
	private int any;
	private String matricula;
	
	public Cotxe() {
		
	}
	
	public Cotxe(String marca, String model, int any, String matricula) {
		super();
		this.marca = marca;
		this.model = model;
		this.any = any;
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getAny() {
		return any;
	}

	public void setAny(int any) {
		this.any = any;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public boolean somethingEquals(Cotxe car) {
		
		if( (matricula.equalsIgnoreCase(car.getMatricula()) || car.getMatricula().equalsIgnoreCase("")) && 
				(marca.equalsIgnoreCase(car.getMarca()) || car.getMarca().equalsIgnoreCase("")) &&
				(model.equalsIgnoreCase(car.getModel()) || car.getModel().equalsIgnoreCase("")) && 
				(any == car.getAny() || car.getAny() == -1)) {
			return true;
		}
		return false;
	}
	
}
