package uf1.exercici2;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class App {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Scanner teclado = new Scanner(System.in);
		Cotxe car = new Cotxe();

		File fitxer = new File("text");
		FileOutputStream fileout = new FileOutputStream(fitxer);
		FileInputStream filein = new FileInputStream(fitxer);
		ObjectOutputStream dataOutCar = new ObjectOutputStream(fileout);
		ObjectInputStream dataInCar = new ObjectInputStream(filein);

		//Escriure en el fitxer automaticament
		EscriureFitxerObject(car, fileout, dataOutCar);

		
		//Men�
		int opcion = -1;
		while (opcion != 0) {
			System.out.println("\n1 - Introduir objecte\n2 - Llegir tots los objecte\n3 - Filtrar objecte\n0 - Sortir");
			opcion = teclado.nextInt();
			switch (opcion) {
			case 1:
				//Introduir manualment
					EscriureFitxerObjectOne(car, dataOutCar);

				break;
			case 2:
				//Llegir tots els cotxes
				LlegirFitxerObject(fitxer, car, filein, dataInCar);

				break;
			case 3:
				//Filtrar cotxes

					LlegirFitxerObjectByAtribute(fitxer, car, filein, dataInCar);
					
				break;
			}
			
		}
		dataInCar.close();
		filein.close();

	}

	public static void EscriureFitxerObject(Cotxe car, FileOutputStream fileout, ObjectOutputStream dataOutCar) throws IOException {

		String marca[] = {"citroen", "renauld", "toyota", "mercedes"};
		String modelo[] = {"sara", "4b", "432", "GTR"};
		int any[] = {21,321,32,32};
		String matricula[] = {"41321BSD", "3213GTX", "EFSFE43", "DADAEATRM"};

		for (int i=0; i<marca.length; i++){
			car = new Cotxe(marca[i], modelo[i], any[i], matricula[i]);
			dataOutCar.writeObject(car);
		}
	}


	public static void EscriureFitxerObjectOne(Cotxe car, ObjectOutputStream dataOutCar) throws IOException {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce un coche manualmente");
		System.out.println("Marca: ");
		car.setMarca(teclado.nextLine());
		System.out.println("Model: ");
		car.setModel(teclado.nextLine());
		System.out.println("Any: ");
		car.setAny(teclado.nextInt());
		System.out.println("Matricula: ");
		teclado.nextLine();
		car.setMatricula(teclado.nextLine());


		dataOutCar.writeObject(car);
		

	}
	
	public static void LlegirFitxerObject(File fitxer, Cotxe car, FileInputStream filein, ObjectInputStream dataInCar) throws IOException, ClassNotFoundException {		
		filein = new FileInputStream(fitxer);
		dataInCar = new ObjectInputStream(filein);

		try {
			while (true){
				car = (Cotxe) dataInCar.readObject();
				System.out.println("Marca: " +car.getMarca() + ", Matricula: "+ car.getMatricula() + ", Model: "+
						car.getModel() + ", Any: "+ car.getAny());
			}
		} catch (EOFException eo) {

		}
		
		dataInCar.close();
		filein.close();
	}

	public static void LlegirFitxerObjectByAtribute(File fitxer, Cotxe car, FileInputStream filein, ObjectInputStream dataInCar) throws IOException, ClassNotFoundException {		
		filein = new FileInputStream(fitxer);
		dataInCar = new ObjectInputStream(filein);

		Cotxe carFilter = new Cotxe();
		Scanner teclado = new Scanner(System.in);

		System.out.println("Introdueix un cotxe per filtrar (Si no vols filtrar per un atribut\n"
				+ " deixa\'l en blanc y en anys posa -1)");
		System.out.println("Marca: ");
		carFilter.setMarca(teclado.nextLine());
		System.out.println("Model: ");
		carFilter.setModel(teclado.nextLine());
		System.out.println("Any: ");
		carFilter.setAny(teclado.nextInt());
		System.out.println("Matricula: ");
		teclado.nextLine();
		carFilter.setMatricula(teclado.nextLine());
		
		
		try {
			while (true){
				car = (Cotxe) dataInCar.readObject();
				if(car.somethingEquals(carFilter)) {
					System.out.println("Marca: " +car.getMarca() + ", Matricula: "+ car.getMatricula() + ", Model: "+
							car.getModel() + ", Any: "+ car.getAny());
				}
			}
		} catch (EOFException eo) {
		}
		
		dataInCar.close();
		filein.close();

	}
	
}