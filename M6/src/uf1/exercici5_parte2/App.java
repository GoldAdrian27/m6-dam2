package uf1.exercici5_parte2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class App {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File file = new File("./src/uf1/exercici5_parte2/arxiuxml.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		Node element = doc.getDocumentElement();
		Scanner teclado = new Scanner(System.in);
		boolean isRunning = true;
		int respuesta;
		recargarId(element);
		// Men�
		while (isRunning) {
			System.out.println("Que deseas hacer?");

			System.out.println("1 - Buscar elemento");
			System.out.println("2 - Eliminar elemento");
			System.out.println("3 - Modificar elemento");
			System.out.println("4 - A�adir elemento row");
			System.out.println("5 - Mostrar el documento");
			System.out.println("6 - Guardar");
			System.out.println("0 - Salir");

			respuesta = teclado.nextInt();

			switch (respuesta) {
			case 1:
				System.out.println("Indica la id por la que quieres buscar");
				buscarElementRow(doc);
				break;
			case 2:
				eliminarElementRow(doc);
				break;
			case 3:
				modificarElemento(doc);
				break;
			case 4:
				a�adirRow(doc);
				break;
			case 5:
				mostrarXML(element);
				break;
			case 6:
				guardar(doc);
				break;

			default:
				isRunning = false;
				break;

			}
		}

	}
	public static void buscarElementRow(Document doc) {

		Node nodeArrel = doc.getDocumentElement();
		Scanner teclado = new Scanner(System.in);
		System.out.println("Escribe la id del ElementRow a mostrar");
		String id = teclado.next();
		//busco un elemento
		Element elementToFind = doc.getElementById(id);
		//Si existe lo mostrara
		if(elementToFind != null) {
			mostrarXML(elementToFind);
		}else {
			System.out.println("No existe el elemento con esa id");
		}
	}
	public static void eliminarElementRow(Document doc) {
		//Elijo el elemento que contiene todos los elementos necesarios
		Node nodeArrel = doc.getDocumentElement().getChildNodes().item(0);
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce la id del elemento a eliminar");
		String id = teclado.next();
		//Selecciono la id
		Element elementToDelete = doc.getElementById(id);
		//Si existe lo eliminara
		if(elementToDelete != null) {
			nodeArrel.removeChild(elementToDelete);
		}else {
			System.out.println("No existe el elemento con esa id");
		}
	}
	public static void recargarId(Node nodeArrel) {
		//Selecciono el nodo donde contiene los elementos con id
		NodeList child = nodeArrel.getChildNodes().item(0).getChildNodes();

		for (int i = 0; i < child.getLength(); i++) {
			//Si el elemento se llama row y contiene atributos los guardara y lo
			//buscara el atributo _id para decirle que es el identificador
			if (child.item(i).getNodeName().equals("row") && child.item(i).hasAttributes()) {
				NamedNodeMap atributs = child.item(i).getAttributes();
				for (int j = 0; j < atributs.getLength(); j++) {
					if (atributs.item(j).getNodeName().equals("_id")) {
						String numId = atributs.item(j).getNodeValue();
						((Element) child.item(i)).setIdAttribute("_id", true);
					}
				}
			}
		}
	}

	public static void a�adirRow(Document doc) {
		Node element = doc.getDocumentElement().getFirstChild();
		Element childElement;
		Node text;
		Scanner teclado = new Scanner(System.in);
		Element newElementRow = doc.createElement("row");
		Element lastElementRow = getLastChildElement(element);
		//La id ira incrementando un "1" al id del ultimo atributo para que sea automatico
		String idLastElementRow = String.valueOf((lastElementRow.getAttributes().getNamedItem("_id").getNodeValue())) + "1";
		newElementRow.setAttribute("_id", idLastElementRow);
		newElementRow.setIdAttribute("_id", true);
		//pediremos todos los atributos y nodos necesarios
		System.out.println("Escribe la posicion:");
		newElementRow.setAttribute("_position", teclado.next());

		System.out.println("Escribe el uuid:");
		newElementRow.setAttribute("_uuid", teclado.next());

		System.out.println("Escribe la direccion electronica:");
		newElementRow.setAttribute("_adress", teclado.next());

		String[] childNodeName = { "data", "total_de_casos_confirmats", "total_de_defuncions"};
		System.out.println("Escribe el la fecha");
		String data = teclado.next();
		System.out.println("Escribe el total de confirmados con covid");
		String total_de_casos_confirmats = teclado.next();
		System.out.println("Escribe el total de defuncions");
		String total_de_defuncions = teclado.next();
		String[] childNodeValue = { data, total_de_casos_confirmats, total_de_defuncions };

		//guardo cada uno de los elementos en el elemento
		for (int i = 0; i < childNodeName.length; i++) {
			childElement = doc.createElement(childNodeName[i]);
			text = doc.createTextNode(childNodeValue[i]);
			childElement.appendChild(text);
			newElementRow.appendChild(childElement);
		}

		element.appendChild(newElementRow);

	}

	public static void guardar(Document doc) {
		try {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			// send DOM to file
			tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream("./src/uf1/exercici5_parte2/arxiuxml.xml")));

		} catch (TransformerException te) {
			System.out.println(te.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	public static void mostrarXML(Node node) {
		//muestra el nombre del nodo
		System.out.println("Elemento: " + node.getNodeName());
		//muestra los atributos
		NamedNodeMap atributos = node.getAttributes();

		System.out.println("\tAtributos:");
		for (int i = 0; i < atributos.getLength(); i++) {
			System.out.println("\t\t" + atributos.item(i));
		}
		//muestra los textos
		System.out.print("\tTexto: ");
		if (node.getChildNodes().item(0) != null && node.getChildNodes().item(0).getNodeType() == Node.TEXT_NODE) {
			String texto = node.getChildNodes().item(0).getNodeValue();
			System.out.println(texto);
		}else {
			System.out.println();
		}

		NodeList nodeList = node.getChildNodes();

		for (int i = 0; i < nodeList.getLength(); i++) {
			//busca el siguiente nodo
			if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
				mostrarXML(nodeList.item(i));
			}
		}
	}

	public static Element getLastChildElement(Node node) {
		//Selecciona el ultimo elemento 
		node = node.getLastChild();
		while (node != null && node.getNodeType() != Node.ELEMENT_NODE) {
			node = node.getPreviousSibling();
		}
		return (Element) node;
	}

	public static void modificarElemento(Document doc) {
		Scanner teclado = new Scanner(System.in);
		//Seleccionar Elemento
		System.out.println("Selecciona el elemento que deseas modificar por la id");
		Element elementToFind = doc.getElementById(teclado.next());
		boolean isRunning = true;
		int respuesta;

		while (isRunning) {
			System.out.println("Que deseas hacer en este elemento row");

			System.out.println("1 - Eliminar elemento al elemento row");
			System.out.println("2 - A�adir elemento al elemento row");
			System.out.println("3 - Modificar elemento al elemento row");
			System.out.println("4 - Mostrar elemento row");
			System.out.println("0 - Salir");

			respuesta = teclado.nextInt();

			switch (respuesta) {
			case 1:
				System.out.println("Escribe el elemento que quieres eliminar");
				String elementToDelete = teclado.next();
				//selecciono el elemento a eliminar
				Node nodeToDelete = elementToFind.getElementsByTagName(elementToDelete).item(0);
				//Si existe lo elimino
				if(nodeToDelete != null) {
				elementToFind.removeChild(nodeToDelete);
				}else {
					System.out.println("Ese elemento no existe");
				}
				break;
			case 2:
				System.out.println("Escribe el nombre del nuevo nodo");
				String newElementName = teclado.next();
				//Creo un nuevo nodo
				Element newElement = doc.createElement(newElementName);
				elementToFind.appendChild(newElement);
				break;
			case 3:
				System.out.println("Escribe el elemento que quieres modificar");
				String elementToModify = teclado.next();
				//selecciono el elemento a modificar
				Node nodeToModify = elementToFind.getElementsByTagName(elementToModify).item(0);
				//si existe le pido que introduzca que quiere que tenga ese elemento
				if(nodeToModify != null) {
				System.out.println("Que texto le quieres a�adir");
				String newTextToElement = teclado.next();
				//creo el elemento texto y lo a�ado
				Node text = doc.createTextNode(newTextToElement);
				nodeToModify.appendChild(text);
				//en caso de haber una modificacion anteriormente lo modificar�
				nodeToModify.getChildNodes().item(0).setNodeValue(newTextToElement);
				}else {
					System.out.println("Ese elemento no existe");
				}
				break;
			case 4:

				mostrarXML(elementToFind);
				break;

			default:
				isRunning = false;
				break;
			}
		}
	}
}