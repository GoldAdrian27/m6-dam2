package uf1.exercici4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class App {
	//setAtributte("ID", "1")
	//setIdAtributte("ID", true)
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File file = new File("./src/uf1/exercici4/arxiuxml.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		Node nodeArrel = doc.getDocumentElement();
		
		recursividad(nodeArrel);
	}
	
	
	public static void recursividad(Node node) {
		
	    System.out.println("Node: " + node.getNodeName());
		
	    NamedNodeMap atributos = node.getAttributes();
	    
	    System.out.println("\tAtributos:" );
	    for (int i = 0; i < atributos.getLength(); i++) {
			System.out.println("\t\t" + atributos.item(i));
		}
	    
	    System.out.print("\tTexto: ");
		    String texto = node.getTextContent();
		    System.out.println(texto);
	    
	    
	    NodeList nodeList = node.getChildNodes();
	    
	    for (int i = 0; i < nodeList.getLength(); i++) {
	        
	    	if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
		    	recursividad(nodeList.item(i));	        	
	        }
		}
		
	}

}