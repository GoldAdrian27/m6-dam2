package uf1.exercici3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class App {

	public static void main(String[] args) throws IOException {
		File fitxer = new File("persones");
		RandomAccessFile aleatoriFile = new RandomAccessFile(fitxer, "rw");
		
		String nom[] = {"Adrian", "Jose", "Manuel", "Marco"};
		String cognom[] = {"Jurado", "Cortes", "Gonzalez", "Garcia"};
		int edad[] = {23, 12, 34, 32};
		String dni[] = {"32132133F", "2313123D", "3213123G", "3213123P", "21312323F"};
		StringBuffer buffer;

		for (int i=0; i<nom.length; i++) {
			aleatoriFile.writeInt(i+1);
			
			buffer = new StringBuffer (nom[i]);
			buffer.setLength(50);
			aleatoriFile.writeChars(buffer.toString());
			
			buffer = new StringBuffer (cognom[i]);
			buffer.setLength(50);
			aleatoriFile.writeChars(buffer.toString());

			aleatoriFile.writeInt(edad[i]);
			
			buffer = new StringBuffer (dni[i]);
			buffer.setLength(25);
			aleatoriFile.writeChars(buffer.toString());
			
		}
		aleatoriFile.close();
		System.out.println("Llegir tot el fitxer");
		read(fitxer);
		System.out.println("Buscar per la id 3");
		buscar(fitxer, 3);
	}





		public static void read( File fitxer) throws IOException {
		RandomAccessFile aleatoriFile = new RandomAccessFile(fitxer, "r");

		int apuntador = 0;
		int edadR;
		int idR;
		char nomR[] = new char[50];
		char cognomR[] = new char[50];
		char dniR[] = new char[25];
		char aux;

		while (!(aleatoriFile.getFilePointer() == aleatoriFile.length())) {
			aleatoriFile.seek(apuntador);
			
			//Llegeix ID
			idR = aleatoriFile.readInt();
			
			for(int i = 0; i<nomR.length; i++) {
				aux = aleatoriFile.readChar();
				nomR[i] = aux;
			}
			String noms = new String(nomR);
			
			
			for(int i = 0; i<cognomR.length; i++) {
				aux = aleatoriFile.readChar();
				cognomR[i] = aux;
			}
			String cognoms = new String(cognomR);
			
			edadR = aleatoriFile.readInt();

			for(int i = 0; i<dniR.length; i++) {
				aux = aleatoriFile.readChar();
				dniR[i] = aux;
			}
			String dnis = new String(dniR);
			
			
			//Sortida de les dades de cada llibre
			System.out.println("ID: "+idR+"\nNom: "+noms+"\nEdad: "+edadR+"\nCognom: "+cognoms+"\nDNI:"+dnis +"\n\n");
			
			//S'ha de posicionar l'apuntador al seg�ent llibre
			apuntador += 258;
			
			//Si coincideix on s'est� apuntat amb el final del fitxer, sortim
		}
		aleatoriFile.close();//Tancar el fitxer

	}
	
	public static void buscar(File fitxer, int idBuscar) throws IOException {
		RandomAccessFile aleatoriFile = new RandomAccessFile(fitxer, "r");

		int apuntador = 0;
		int edadR;
		int idR;
		char nomR[] = new char[50];
		char cognomR[] = new char[50];
		char dniR[] = new char[25];
		char aux;

		while (!(aleatoriFile.getFilePointer() == aleatoriFile.length())) {
			aleatoriFile.seek(apuntador);
			
			//Llegeix ID
			idR = aleatoriFile.readInt();
			
			for(int i = 0; i<nomR.length; i++) {
				aux = aleatoriFile.readChar();
				nomR[i] = aux;
			}
			String noms = new String(nomR);
			
			
			for(int i = 0; i<cognomR.length; i++) {
				aux = aleatoriFile.readChar();
				cognomR[i] = aux;
			}
			String cognoms = new String(cognomR);
			
			edadR = aleatoriFile.readInt();

			for(int i = 0; i<dniR.length; i++) {
				aux = aleatoriFile.readChar();
				dniR[i] = aux;
			}
			String dnis = new String(dniR);
			
			if(idR == idBuscar) {
				System.out.println("ID: "+idR+"\nNom: "+noms+"\nEdad: "+edadR+"\nCognom: "+cognoms+"\nDNI:"+dnis +"\n\n");
			} 			
			apuntador += 258;
			
		}
		aleatoriFile.close();//Tancar el fitxer

	}

}
