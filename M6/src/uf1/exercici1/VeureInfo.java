package uf1.exercici1;

import java.io.*;

public class VeureInfo {
	public static void main(String[] args) {
		File f = new File("src" + File.separator + "uf1" + File.separator + "exercici1" + File.separator + args[1]);
		System.out.println("Esta ocult: " + f.isHidden());
		if(f.exists()) {
			if(f.isFile()){
				 System.out.println("Nom del fitxer : "+f.getName());
				 System.out.println("Ruta           : "+f.getPath());
				 System.out.println("Ruta absoluta  : "+f.getAbsolutePath());
				 System.out.println("Es pot escriure: "+f.canRead());
				 System.out.println("Es pot llegir  : "+f.canWrite());
				 System.out.println("Grandaria      : "+f.length());
				 System.out.println("Es un directori: "+f.isDirectory());
				 System.out.println("Es un fitxer   : "+f.isFile());
			}else if(f.isDirectory()){
				 String[] arxius = f.list();
				 for (int i = 0; i<arxius.length; i++){
					 System.out.println(arxius[i]);
				 }
			}
		}

	}
}